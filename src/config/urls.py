from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from core.views import LMSHome, Error404
from students.views import StudentSearchView

urlpatterns = [
                  path("admin/", admin.site.urls),
                  path("", LMSHome.as_view(), name='index'),
                  path("", include("core.urls")),
                  path("search/", StudentSearchView.as_view(), name="searchbar"),
                  path("students/", include("students.urls")),
                  path("teachers/", include("teachers.urls")),
                  path("groups/", include("groups.urls")),
                  path("__debug__/", include("debug_toolbar.urls")),
                  path("oauth/", include("social_django.urls", namespace="social")),
                  path('accounts/', include('phone_auth.urls', namespace="phone_auth")),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

handler404 = Error404.as_view()
