from django.contrib import admin  # NOQA
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from core.models import Profile


class ProfileAdminInline(admin.StackedInline):
    model = Profile
    extra = 0


@admin.register(get_user_model())
class CustomerAdmin(UserAdmin):
    inlines = [ProfileAdminInline]
    fields = ("first_name", "last_name", "password", "email")
    fieldsets = None
    list_display = ("first_name", "last_name", "email")
    ordering = ["email"]
    readonly_fields = ("email",)
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'first_name', 'last_name',),
        }),
    )


