from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm, AuthenticationForm
from django.forms import ModelForm
from phonenumber_field.formfields import PhoneNumberField
from core.models import Profile


class LoginForm(AuthenticationForm):
    contact_email = forms.EmailField(
        error_messages={'invalid': 'This is my email error msg.'},
        widget=forms.TextInput(attrs={'class': 'form-control', 'autocomplete': 'off'}),
        required=True)
    contact_phone = PhoneNumberField(required=True)


class RegistrationForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ("first_name", "phone_number", "email", "password1", "password2")


class PasswordChangingForm(PasswordChangeForm):
    old_password = forms.CharField(widget=forms.PasswordInput(attrs={"class": "form-control", "type": "password"}))
    new_password1 = forms.CharField(max_length=100,
                                    widget=forms.PasswordInput(attrs={"class": "form-control", "type": "password"}))
    new_password2 = forms.CharField(max_length=100,
                                    widget=forms.PasswordInput(attrs={"class": "form-control", "type": "password"}))

    class Meta:
        model = get_user_model()
        fields = ("old_password", "new_password1", "new_password2")


class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ["user_type", "avatar", "birth_date", "email", "phone_number", "location", "web_site", "github"]
