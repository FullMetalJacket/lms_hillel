import datetime
import logging

LOGGER = logging.getLogger("custom_logger")


class CustomMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        LOGGER.debug(f"Before request {request.user}")
        setattr(request, "current_time", datetime.datetime.now())
        response = self.get_response(request)
        print("After request")

        return response
