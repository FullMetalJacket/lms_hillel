from django.urls import path
from core.views import (Login, Logout, Registration, PasswordsChangeView, ActivateUser, PasswordSuccessView,
                        ProfileDetailView, ProfileUpdateView)

app_name = "core"

urlpatterns = [
    path("login/", Login.as_view(), name="login"),
    path("logout/", Logout.as_view(), name="logout"),
    path("registration/", Registration.as_view(), name="registration"),
    path("password/",
         PasswordsChangeView.as_view(template_name="registration/change-password.html"), name="password"),
    path("password_success/", PasswordSuccessView.as_view(), name="password_success"),
    path("activate/<str:uuid64>/<str:token>/", ActivateUser.as_view(), name="activate_user"),
    path("profile/<int:pk>", ProfileDetailView.as_view(), name="profile"),
    path("profile/<int:pk>/update", ProfileUpdateView.as_view(), name="profile-update"),

]
