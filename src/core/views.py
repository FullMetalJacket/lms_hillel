from django.contrib import messages
from django.contrib.auth import get_user_model, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.http import HttpResponse
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.views.generic import TemplateView, CreateView, RedirectView, UpdateView, DetailView

from core.forms import RegistrationForm, PasswordChangingForm, ProfileForm
from core.models import Profile
from core.services.emails import send_registration_email
from core.utils.token_generator import TokenGenerator


class LMSHome(TemplateView):
    template_name = "index.html"
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        print(f"In request: {request.current_time}")
        if request.user.is_authenticated:
            messages.warning(request, "User is available")
        else:
            messages.info(request, "Please login")

        return super().get(self, request, *args, **kwargs)


class Error404(TemplateView):
    template_name = "errors/not-found.html"


class Login(LoginView):
    next_page = reverse_lazy("index")


class Logout(LogoutView):
    next_page = reverse_lazy("core:login")


class Registration(CreateView):
    template_name = "registration/create_user.html"
    form_class = RegistrationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()
        send_registration_email(request=self.request, user_instance=self.object)
        return super().form_valid(form)


class PasswordsChangeView(PasswordChangeView):
    form_class = PasswordChangingForm
    success_url = reverse_lazy("core:password_success")


class PasswordSuccessView(TemplateView):
    template_name = reverse_lazy("registration/password-success.html")


class ActivateUser(RedirectView):
    url = reverse_lazy("index")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, TypeError, ValueError):
            return HttpResponse("Wrong data")

        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()

            login(request, current_user)
            return super().get(request, *args, **kwargs)

        return HttpResponse("Wrong data")


class ProfileDetailView(LoginRequiredMixin, DetailView):
    model = Profile
    template_name = "pages/profile-page.html"


class ProfileUpdateView(LoginRequiredMixin, UpdateView):
    model = Profile
    form_class = ProfileForm
    template_name = "tools/profile-edit.html"
    login_url = "login"

    def get_success_url(self):
        return reverse_lazy("core:profile", kwargs={'pk': self.kwargs['pk']})
