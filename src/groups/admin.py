from django.contrib import admin
from django import forms
from groups.models import Group, Lesson
from students.admin import StudentAdminInline
from teachers.admin import TeacherAdminInline, count_to_zero


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = "__all__"

    comments = forms.CharField(required=False, widget=forms.Textarea)
    email = forms.EmailField(required=True)
    send_email = forms.BooleanField(required=False)

    def clean_count_of_students(self):
        if self.cleaned_data["count_of_students"] > 100:
            raise forms.ValidationError("Should be less than 100!!!")
        return self.cleaned_data["count_of_students"]

    def save(self, commit=True):
        print(self.cleaned_data["comments"])
        print(self.cleaned_data["email"])
        # TODO send email
        return super().save(commit=commit)


class InLineLesson(admin.TabularInline):
    model = Lesson
    extra = 1
    max_num = 15


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    inlines = [StudentAdminInline, TeacherAdminInline, InLineLesson]
    actions = [count_to_zero]
    form = GroupForm
    list_filter = ("group_name", "amount_student", "teacher_name", "group_number", "start_date", "end_date")
#
