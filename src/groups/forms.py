from django.core.exceptions import ValidationError
from django.forms import ModelForm

from groups.models import Group


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = "__all__"

    @staticmethod
    def normalize_text(text: str) -> str:
        return text.strip().capitalize()

    def clean_group_name(self):
        return self.normalize_text(self.cleaned_data["group_name"])

    def clean_department_email(self):
        email = self.cleaned_data["group_email"]
        if ".ru" in email.lower():
            raise ValidationError("You can't use .ru in email")
        return email

    def clean_department_phone(self):
        department_phone = self.cleaned_data["group_phone"]
        if not department_phone.isdigit():
            raise ValidationError("Please enter a valid phone number")
        return department_phone
