import random
from faker import Faker
from django.db import models

from phonenumber_field.modelfields import PhoneNumberField


class Group(models.Model):
    group_name = models.TextField()
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    group_email = models.EmailField(max_length=100)
    group_phone = PhoneNumberField()
    amount_student = models.PositiveSmallIntegerField(default=5, null=True)
    teacher_name = models.CharField(max_length=100, null=True)
    group_number = models.IntegerField(null=True)

    def __str__(self) -> str:
        return f"{self.group_name}, {self.group_phone}, {self.group_email}"

    @classmethod
    def generate_instances(cls, count: int) -> None:
        faker = Faker()

        for _ in range(count):
            cls.objects.create(
                group_name=faker.company_suffix(),
                group_email=faker.email(),
                group_phone=faker.phone_number(),
                start_date=faker.date_time_between(start_date="-10d"),
                end_date=faker.date_time_between(start_date="-10d"),
                amount_student=random.randint(10, 15),
                teacher_name=faker.name(),
                group_number=1,

            )


class Lesson(models.Model):
    title = models.CharField(max_length=120)
    course = models.ForeignKey(Group, on_delete=models.SET_NULL, null=True)
    position = models.IntegerField()
    video_url = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.title
