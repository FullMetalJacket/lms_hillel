from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.views.generic import (CreateView, UpdateView, DeleteView, ListView)

from groups.models import Group
from groups.forms import GroupForm


class GroupGetView(LoginRequiredMixin, ListView):
    model = Group
    template_name = "groups/groups_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        groups = Group.objects.all()
        context["groups"] = groups
        return context


class CreateGroupView(LoginRequiredMixin, CreateView):
    model = Group
    form_class = GroupForm
    template_name = "groups/groups_create.html"
    success_url = reverse_lazy("groups:get_groups")


class UpdateGroupView(LoginRequiredMixin, UpdateView):
    model = Group
    form_class = GroupForm
    template_name = "groups/groups_update.html"
    success_url = reverse_lazy("groups:get_groups")


class DeleteGroupView(LoginRequiredMixin, DeleteView):
    model = Group
    success_url = reverse_lazy('groups:get_groups')

    def get_object(self, *args, **kwargs):
        return get_object_or_404(Group, pk=self.request.POST.get('pk'))
