from django.contrib import admin
from django.utils.safestring import mark_safe

from students.models import Student


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    date_hierarchy = "birth_date"
    ordering = (
        "first_name",
        "last_name",
    )
    list_display = ("avatar_tag", "first_name", "last_name", "grade", "email", "age", "birth_date")
    list_editable = ["grade"]
    list_filter = (
        "group",
        "grade",
    )
    search_fields = ("first_name__istartswith", "email")

    def avatar_tag(self, obj):
        if obj.avatar:
            return mark_safe(f"<img src='{obj.avatar.url}' width='32'>")
        return mark_safe("<img src=/static/img/default_avatar.jpg>")

    avatar_tag.short_description = "Avatar"


class StudentAdminInline(admin.StackedInline):
    model = Student
    extra = 0
