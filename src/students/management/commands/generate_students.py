from django.core.management.base import BaseCommand
from faker import Faker


fake = Faker()


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("generate_students", type=int, default=10)

    def handle(self, *args, **options):
        count = options["generate_students"]
        for i in range(count):
            name = fake.name()
            email = fake.email()
            self.stdout.write(f"Student: {name}, email: {email}\n")
