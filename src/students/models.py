from django.db import models
from django.core import validators

from faker import Faker

from core.models import Person


class Student(Person):
    grade = models.PositiveSmallIntegerField(default=0, null=True)
    resume = models.FileField(upload_to="docs", validators=[
        validators.FileExtensionValidator(["pdf", "doc", "docx"],
                                          message="Not correct file type, you can upload only - PDF or DOC files")],
                              null=True)
    group = models.ForeignKey(to="groups.Group", on_delete=models.CASCADE, related_name="students", null=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email}"

    @classmethod
    def generate_instances(cls, count: int) -> None:
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_time_between(start_date="-25y", end_date="-18y"),
                phone_number=faker.phone_number(),
                grade=faker.random.randint(0, 100),
            )
