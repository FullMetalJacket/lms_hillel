from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import (CreateView, UpdateView, DeleteView, ListView)
from django.contrib.auth.mixins import LoginRequiredMixin

from students.models import Student
from students.forms import StudentForm


class StudentGetView(LoginRequiredMixin, ListView):
    model = Student
    template_name = "students/students_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        students = Student.objects.all()
        context["students"] = students
        return context


class CreateStudentView(LoginRequiredMixin, CreateView):
    model = Student
    form_class = StudentForm
    template_name = "students/students_create.html"
    success_url = reverse_lazy("students:get_students")


class UpdateStudentView(LoginRequiredMixin, UpdateView):
    model = Student
    form_class = StudentForm
    template_name = "students/students_update.html"
    success_url = reverse_lazy("students:get_students")


class DeleteStudentView(LoginRequiredMixin, DeleteView):
    model = Student
    success_url = reverse_lazy("students:get_students")

    def get_object(self, *args, **kwargs):
        return get_object_or_404(Student, pk=self.request.POST.get("pk"))


class StudentSearchView(LoginRequiredMixin, ListView):
    model = Student
    template_name = "tools/searchbar.html"

    def get_queryset(self):
        q = self.request.GET.get("q")
        object_list = Student.objects.filter(Q(first_name__icontains=q) | Q(last_name__icontains=q))
        return object_list
