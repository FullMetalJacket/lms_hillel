from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from teachers.models import Teacher


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ("avatar_tag", "first_name", "last_name", "email", "age", "birth_date", "group_count", "links_to_groups")
    list_filter = ("group", "first_name", "last_name", "email", "phone_number", "birth_date")
    fieldsets = (
        (
            "Personal info",
            {"fields": ("first_name", "last_name", "email")},
        ),
        (
            "Additional info",
            {
                "classes": ("collapse",),
                "fields": (
                    "birth_date",
                    "group",
                ),
            },
        ),
    )
    search_fields = ("first_name__istartswith", "email")

    def avatar_tag(self, obj):
        if obj.avatar:
            return mark_safe(f"<img src='{obj.avatar.url}' width='32'>")
        return mark_safe("<img src=/static/img/default_avatar.jpg>")

    avatar_tag.short_description = "Avatar"

    def group_count(self, obj):
        if obj.group:
            return obj.group.count()
        return 0

    def links_to_groups(self, obj):
        if obj.group:
            groups = obj.group.all()
            links = []
            for group in groups:
                links.append(
                    f"<a class='button' href='"
                    f"{reverse('admin:groups_group_change', args=[group.pk])}'"
                    f">{group.group_name}</a>"
                )
            return format_html("</br>".join(links))
        return "No groups"


@admin.action(description="Set to count of selected groups")
def count_to_zero(models_admin, request, queryset):
    queryset.update(count_of_students=0)


class TeacherAdminInline(admin.StackedInline):
    model = Teacher.group.through
    extra = 0
