# Generated by Django 3.2 on 2022-09-10 13:34

from django.db import migrations, models
import django.utils.timezone
import phonenumber_field.modelfields
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('groups', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('uuid', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False, primary_key=True, serialize=False, unique=True)),
                ('avatar', models.ImageField(blank=True, null=True, upload_to='img')),
                ('first_name', models.CharField(max_length=100, null=True)),
                ('last_name', models.CharField(max_length=100, null=True)),
                ('email', models.EmailField(max_length=100, null=True)),
                ('phone_number', phonenumber_field.modelfields.PhoneNumberField(max_length=128, region=None)),
                ('birth_date', models.DateField(default=django.utils.timezone.now, null=True)),
                ('group', models.ManyToManyField(to='groups.Group')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
