from django.db import models

from faker import Faker

from core.models import Person


class Teacher(Person):
    group = models.ManyToManyField("groups.Group")

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email}"

    @classmethod
    def generate_instance(cls, count: int) -> None:
        faker = Faker()

        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                phone_number=faker.phone_number(),
                birth_date=faker.date_time_between(start_date="-60y", end_date="-30y"),
            )
