from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.views.generic import (CreateView, UpdateView, DeleteView, ListView)
from django.contrib.auth.mixins import LoginRequiredMixin

from teachers.models import Teacher
from teachers.forms import TeacherForm


class TeacherGetView(LoginRequiredMixin, ListView):
    model = Teacher
    template_name = "teachers/teachers_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        teachers = Teacher.objects.all()
        context["teachers"] = teachers
        return context


class CreateTeacherView(LoginRequiredMixin, CreateView):
    model = Teacher
    form_class = TeacherForm
    template_name = "teachers/teachers_create.html"
    success_url = reverse_lazy("teachers:get_teachers")


class UpdateTeacherView(LoginRequiredMixin, UpdateView):
    model = Teacher
    form_class = TeacherForm
    template_name = "teachers/teachers_update.html"
    success_url = reverse_lazy("teachers:get_teachers")


class DeleteTeacherView(LoginRequiredMixin, DeleteView):
    model = Teacher
    success_url = reverse_lazy("teachers:get_teachers")

    def get_object(self, *args, **kwargs):
        return get_object_or_404(Teacher, pk=self.request.POST.get("pk"))
